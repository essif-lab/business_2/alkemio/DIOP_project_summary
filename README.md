# Background
Our society faces many challenges, and solving these challenges requires many parties to collaborate. 

However our efforts at solving these challenges are often fragmented, and it is hard to connect the people and ideas needed to make progress. In short, unless we can step up our game on [UN SDG](https://sdgs.un.org/goals) #17 we are going to struggle with all the other SDGs. We need a fundamentally different approach. 

# Challenge Centric
Alkemio puts Challenges central. This starts by having a clear shared understanding of where we are now and where we wish to go i.e. what is the change to be achieved. Around this shared understanding, "Context", build up a "Community" of users, organisations and knowledge that can contribute. The Community can then Collaborate and Coordinate to make progress on the Challenge. 

This is a simple yet fundamental change: it is not organisations or users that are central but the change that we wish to achieve. 

## Decentralization 
A Challenge centric platform needs to have trust as a core principle, so that the collaborating parties can interact with each other with confidence in the fairness of the platform. This implies that the wider trust framework has to be clear, and that parties can interact with each other in a self-sovereign way. 

Alkemio has been designed to be ready for a decentralized future. Authorization is based on (non-verified) Credentials, all core entities in the platform have Agents that manage the Credentials. Important to note that these Credentials are all platform issued - so non-verified.

# Scope
This project is about how to hook into existing trust frameworks, SSI Assurance Communities, and enable Verified Credentials to be used in the platform interactions. 

There are two use cases that will be worked on together with the Digital Innovation Team from The Hague:
* Citizen Participation: allowing citizens holding a VC in a local wallet to be able to interact on the basis of that credential.
* Debt Relief: allowing citizens to interact on innovations related to deb relief based on the holding of a specific VC that they are eligible for debt relief. 

The platform will be extended to allow registration and usage of externally issued Verified Credentials in collaboration workflows. 


